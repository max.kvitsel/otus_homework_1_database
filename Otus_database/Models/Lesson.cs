﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Otus_database.Models
{
    public partial class Lesson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? IdCourse { get; set; }

        public virtual Course IdCourseNavigation { get; set; }

        public override string ToString() => $"{Id}, {Name}, {IdCourseNavigation.Name}";
    }
}
