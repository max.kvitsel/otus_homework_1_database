﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_database.Models
{
    public class Repository
    {
        private readonly OtusContext _dbContext;

        public Repository(OtusContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Course> GetAllCourses() =>
            _dbContext.Courses.ToList();

        public List<Lesson> GetAllLessons() =>
            _dbContext.Lessons.ToList();

        public List<Student> GetAllStudents() =>
            _dbContext.Students.ToList();


        public void AddCourse()
        {
            Console.WriteLine("Enter new course name");
            Course course = new Course();
            course.Name = Console.ReadLine();
            _dbContext.Courses.Add(course);
            _dbContext.SaveChanges();
        }

        public void AddLesson()
        {
            Console.WriteLine("Enter new lesson name");
            Lesson lesson = new Lesson();
            lesson.Name = Console.ReadLine();
            Console.WriteLine("Enter ID course for lesson");
            lesson.IdCourse = Convert.ToInt32(Console.ReadLine());
            _dbContext.Lessons.Add(lesson);
            _dbContext.SaveChanges();
        }

        public void AddStudent()
        {
            Console.WriteLine("Enter student first name");
            Student student = new Student();
            student.Firstname = Console.ReadLine();
            Console.WriteLine("Enter student last name");
            student.Lastname = Console.ReadLine();
            Console.WriteLine("Enter student age");
            student.Age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter ID course for student");
            student.IdCourse = Convert.ToInt32(Console.ReadLine());
            _dbContext.Students.Add(student);
            _dbContext.SaveChanges();
        }
    }
}
