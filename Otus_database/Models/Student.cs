﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Otus_database.Models
{
    public partial class Student
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int? Age { get; set; }
        public string Email { get; set; }
        public int? IdCourse { get; set; }

        public virtual Course IdCourseNavigation { get; set; }

        public override string ToString() => $"{Id}, {Firstname} {Lastname}, {Age}, {Email}, {IdCourseNavigation.Name}";
    }
}
