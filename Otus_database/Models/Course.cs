﻿using System;
using System.Collections.Generic;
using System.Linq;

#nullable disable

namespace Otus_database.Models
{
    public partial class Course
    {
        public Course()
        {
            Lessons = new HashSet<Lesson>();
            Students = new HashSet<Student>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Lesson> Lessons { get; set; }
        public virtual ICollection<Student> Students { get; set; }

        public override string ToString() => $"{Id}, {Name}";
    }
}
