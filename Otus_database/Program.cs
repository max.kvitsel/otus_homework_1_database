﻿using Microsoft.EntityFrameworkCore;
using Otus_database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus_database
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Repository repository = new Repository(new OtusContext());

            Console.WriteLine("Courses:");
            Print<Course, List<Course>>(repository.GetAllCourses());

            Console.WriteLine("---");

            Console.WriteLine("Lessons:");
            Print<Lesson, List<Lesson>>(repository.GetAllLessons());

            Console.WriteLine("---");

            Console.WriteLine("Students:");
            Print<Student, List<Student>>(repository.GetAllStudents());

            Console.WriteLine("---");

            Console.WriteLine("To add data in table enter number: 1 - Course, 2- Lessons, 3 - Student");
            string selector = Console.ReadLine();
            switch (selector)
            {
                case "1":
                    repository.AddCourse();
                    break;
                case "2":
                    repository.AddLesson();
                    break;
                case"3":
                    repository.AddStudent();
                    break;
                default:
                    break;
            }

        }

        public static void Print<T, U>(U collection) where U : IEnumerable<T>
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }
    }
}
