﻿create database otus with owner postgres;

create table Course
(
    ID   serial   primary key,
    Name varchar(255)  unique
)

create table Lessons
( 
  ID serial
 ,Name varchar(255) unique
 ,ID_Course int
 ,constraint FK_Lessons_ID_Course_Course foreign key(ID_Course) references Course (ID)
)


create table public.Student 
(
    ID serial primary key
   ,FirstName varchar(255)
   ,LastName varchar(255)
   ,Age int
   ,Email varchar(255) unique
   ,ID_Course int
   ,constraint FK_Student_ID_Course_Course foreign key (ID_Course) references public.Course (ID)
)


insert into public.course (name)
values 
    ('C# Professional')
   ,('Big Data')
   ,('Python')
   ,('FrontEnd')
   ,('ASP.NET')


insert into public.lessons (name, id_course)
values 
    ('Architecture', 1)
   ,('Database', 1)
   ,('CI/CD', 1)
   ,('Python introduction', 3)
   ,('HTML', 4)
   ,('CSS', 4)
   
   
  
insert into public.student (firstname, lastname, age, email, id_course)
values
 ('Petr', 'Ivanov', 30, 'petr.ivanov@gmail.com', 4)
,('Nikita', 'Korbutov', 37, 'nik@gmail.com', 1)
,('Nikita', 'Petrov', 27, 'npetrov@gmail.com', 3)
,('Ivan', 'Gromov', 23, 'i.gromov@gmail.com', 3)
,('Kreg', 'Hek', 33, 'kreghek@gmail.com', 1)
